//
//  DictionaryView.swift
//  Toki Trainer
//
//  Created by maddiefuzz on 10/4/22.
//

import SwiftUI

enum SearchMode {
    case Dictionary
    case Definitions
}

struct DictionaryView: View {
    @ObservedObject var tokiDictViewModel = TokiDictionaryViewModel()
    
    @State private var tokiInput: String = ""
    @State private var selectedPartOfSpeech: String?
    @State private var showGenericLegend: Bool = false
    @State private var advancedSearchEnabled = false
    @State var searchMode: SearchMode = .Dictionary
    
    @FocusState private var searchInputIsForuced: Bool
    
    var body: some View {
        VStack {
            HStack {
                TextField("Search", text: $tokiInput)
                    //.multilineTextAlignment(.center)
                    .textInputAutocapitalization(.never)
                    .disableAutocorrection(true)
                    .padding(8)
                    .onSubmit {
                        filterByInput()
                        //tokiDictViewModel.filterDictionaryEnglishMode(tokiInput)
                }
                Button {
                    hideKeyboard()
                    filterByInput()
                } label: {
                    Image(systemName: "magnifyingglass")
                }
                .buttonStyle(.borderedProminent)

                Button(action: {
                    withAnimation {
                        advancedSearchEnabled.toggle()
                    }
                }, label: {
                    Image(systemName: "slider.horizontal.2.square.on.square")
                })
                .buttonStyle(.bordered)
            }
            .padding([.top, .leading, .trailing], 16)
            if advancedSearchEnabled == true {
                VStack {
                    Text("Search for:")
                    Picker("Language", selection: $searchMode) {
                        Text("Words").tag(SearchMode.Dictionary)
                        Text("Definitions").tag(SearchMode.Definitions)
                    }
                    .pickerStyle(SegmentedPickerStyle())
                    .onTapGesture {
                        if self.searchMode == SearchMode.Dictionary {
                            self.searchMode = SearchMode.Definitions
                        } else {
                            self.searchMode = SearchMode.Dictionary
                        }
                        filterByInput()
                    }
                    Button(action: {
                        print("Legend")
                        showGenericLegend.toggle()
                    }, label: {
                        Text("Legend")
                    })
                    .padding(4)
                }
                .padding([.leading, .trailing], 8)
            }
            List(tokiDictViewModel.dictionary, id: \.word) { entry in
                TokiWordsListEntryView(entry: entry, selectedPartOfSpeech: $selectedPartOfSpeech)
            }
            .sheet(item: $selectedPartOfSpeech) { selectedPOS in
                PartsOfSpeechView(selectedPartOfSpeech: selectedPOS)
            }
            .sheet(isPresented: $showGenericLegend, content: {
                PartsOfSpeechView(selectedPartOfSpeech: nil)
            })
            .onChange(of: tokiInput) {
                filterByInput()
                //tokiDictViewModel.filterDictionaryEnglishMode(newValue)
            }
        }
        .onTapGesture {
            hideKeyboard()
        }
    }
    
    func filterByInput() {
        if self.searchMode == SearchMode.Dictionary {
            tokiDictViewModel.filterDictionary(tokiInput)
        } else {
            tokiDictViewModel.filterDictionaryEnglishMode(tokiInput)
        }
    }
}

extension View {
    func searchOptionsButtonStyle(toggle: Binding<Bool>) -> any PrimitiveButtonStyle {
        if toggle.wrappedValue == true {
            BorderedButtonStyle()
        } else {
            BorderlessButtonStyle()
        }
    }
}


struct DictionaryView_Previews: PreviewProvider {
    
    static var previews: some View {
        DictionaryView().previewLayout(.sizeThatFits).environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
