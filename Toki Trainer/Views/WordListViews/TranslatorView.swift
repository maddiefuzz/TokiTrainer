//
//  TranslatorView.swift
//  Toki Trainer
//
//  Created by maddiefuzz on 10/8/22.
//

import SwiftUI

struct TranslatorView: View {
    @ObservedObject var tokiDictViewModel = TokiDictionaryViewModel()
    @State private var selectedPartOfSpeech: String?
    @State private var tokiInput: String = ""
    @State private var translateToTokiPona: Bool = false
    
    var body: some View {
        VStack {
            HStack {
                TextField("Enter Toki Pona Word or Phrase", text: $tokiInput)
                    .textInputAutocapitalization(.never)
                    .disableAutocorrection(true)
                    .padding(8)
                    .onSubmit {
                        tokiDictViewModel.translatePhrase(tokiInput)
                    }
                Button {
                    hideKeyboard()
                    tokiDictViewModel.translatePhrase(tokiInput)
                } label: {
                    Image(systemName: "magnifyingglass")
                }
                .buttonStyle(.borderedProminent)
            }
            .padding([.top, .leading, .trailing], 16)

            if tokiInput.count == 0 {
                List(tokiDictViewModel.dictionary, id: \.word) { entry in
                    TokiWordsListEntryView(entry: entry, selectedPartOfSpeech: $selectedPartOfSpeech)
                }
            } else {
                List(tokiDictViewModel.translatedDictionary, id: \.header) { section in
                    Section {
                        ForEach(section.subDictionary, id: \.word) { entry in
                            TokiWordsListEntryView(entry: entry, selectedPartOfSpeech: $selectedPartOfSpeech)
                        }
                    } header: {
                        Text(section.header)
                    }
                }
            }
        }
        .sheet(item: $selectedPartOfSpeech) { selectedPOS in
            PartsOfSpeechView(selectedPartOfSpeech: selectedPOS)
        }
        .onChange(of: tokiInput) {
            tokiDictViewModel.translatePhrase(tokiInput)
        }
        .onTapGesture {
            hideKeyboard()
        }
    }
    
    func changeTranslationDirection() {
        translateToTokiPona.toggle()
    }
}

struct TranslatorView_Previews: PreviewProvider {
    
    static var previews: some View {
        TranslatorView().previewLayout(.sizeThatFits).environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
