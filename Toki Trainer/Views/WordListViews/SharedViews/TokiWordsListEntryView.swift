//
//  TokiWordsListEntryView.swift
//  Toki Trainer
//
//  Created by maddiefuzz on 10/8/22.
//

import SwiftUI

struct TokiWordsListEntryView: View {
    @State var entry: TokiDictEntry
    @Binding var selectedPartOfSpeech: String?
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(entry.word)
                .font(.title)
            ForEach(entry.definitions, id: \.pos) { definition in
                HStack(alignment: .top) {
                    Button(action: {
                        self.selectedPartOfSpeech = String(definition.pos)
                    }) {
                        Text(definition.pos)
                            .frame(width: 45, height: 22, alignment: .center)
                            .foregroundColor(.black)
                            .background(Color(K.posColors[definition.pos]!))
                            .cornerRadius(5.0)
                            .padding(4)
                    }
                    .buttonStyle(BorderlessButtonStyle())
                    Text(definition.definition)
                        .fixedSize(horizontal: false, vertical: true)
                        .padding(4)
                }
            }
        }
    }
}

struct TokiWordsListEntryView_Previews: PreviewProvider {
    static var entry = TokiDictionaryViewModel().dictionary[5]
    
    static var previews: some View {
        TokiWordsListEntryView(entry: entry, selectedPartOfSpeech: .constant("n"))
            .previewLayout(.sizeThatFits)
    }
}
