//
//  LanguageDirectionView.swift
//  Toki Trainer
//
//  Created by maddiefuzz on 10/8/22.
//

import SwiftUI

struct LanguageDirectionView: View {
    private var fromText: String = ""
    private var toText: String = ""
    private var fromColor: Color = .black
    private var toColor: Color = .black
    
    init(from: String, to: String, fromColor: Color, toColor: Color) {
        self.fromText = from
        self.toText = to
        self.fromColor = fromColor
        self.toColor = toColor
    }
    
    var body: some View {
        HStack {
            Text(fromText)
                .bold()
                .multilineTextAlignment(.center)
                .foregroundColor(.white)
                .frame(width: 100)
                .background(fromColor)
                .border(fromColor)
                .cornerRadius(5)
            Image(systemName: "chevron.right")
            Text(toText)
                .bold()
                .multilineTextAlignment(.center)
                .foregroundColor(.white)
                .frame(width: 100)
                .background(toColor)
                .border(toColor)
                .cornerRadius(5)
        }
        .padding(.top, 4)
    }
}

struct LanguageDirectionView_Previews: PreviewProvider {
    
    static var previews: some View {
        LanguageDirectionView(from: "English", to: "Toki Pona", fromColor: .blue, toColor: .cyan)
            .previewLayout(.fixed(width: 260, height: 40))
    }
}
