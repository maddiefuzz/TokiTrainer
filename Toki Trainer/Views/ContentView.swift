//
//  ContentView.swift
//  Toki Trainer
//
//  Created by Avery Ada Pace on 11/2/21.
//

import SwiftUI
import StoreKit
import CoreData

extension String: Identifiable {
    public var id: String { self }
}

struct ContentView: View {
    @Environment(\.managedObjectContext) private var viewContext

    @StateObject var transactions: TransactionObserver = TransactionObserver()

    var body: some View {
        TabView {
            DictionaryView()
                .tabItem {
                    Image(systemName: "book")
                    Text("Dictionary")
                }
            TranslatorView()
                .tabItem {
                    Image(systemName: "message")
                    Text("Phrase Lookup")
                }
            FlashCardLessonsView()
                .tabItem {
                    Image(systemName: "character.textbox")
                    Text("Flash Cards")
                }
            ContributeView()
                .tabItem {
                    Image(systemName: "heart.circle.fill")
                    Text("Contribute")
                }
        }
        .environmentObject(transactions)
    }

    func openPartsOfSpeechView() {
        print("Button pressed.")
    }
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
