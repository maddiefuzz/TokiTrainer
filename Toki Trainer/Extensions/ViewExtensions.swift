//
//  ViewExtensions.swift
//  Toki Trainer
//
//  Created by maddiefuzz on 10/28/22.
//

import SwiftUI

extension View {
    func hideKeyboard() {
        let resign = #selector(UIResponder.resignFirstResponder)
        UIApplication.shared.sendAction(resign, to: nil, from: nil, for: nil)
    }
}
