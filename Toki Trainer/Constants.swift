//
//  Constants.swift
//  Toki Trainer
//
//  Created by Avery Ada Pace on 11/2/21.
//

import Foundation
import UIKit
import CoreData

struct K {
    static let posColors = [
        "n": UIColor.systemGreen,
        "mod": UIColor.systemYellow,
        "sep": UIColor.systemPurple,
        "vt": UIColor.systemBlue,
        "vi": UIColor.systemCyan,
        "interj": UIColor.systemRed,
        "prep": UIColor.systemBrown,
        "conj": UIColor.systemBrown,
        "kama": UIColor.systemBrown,
        "cont": UIColor.systemBrown,
        "oth": UIColor.systemBrown,
        "extra": UIColor.systemBrown
    ]

    struct UserDefaults {
        static let donationHearts = "donationHearts"
        static let hasDonated = "hasDonated"
    }

    struct ConsumableTransactions {
        static let TierOne = "SingleTimeTipTierOne"
        static let TierTwo = "SingleTimeTipTierTwo"
        static let TierThree = "SingleTimeTipTierThree"
        static let TierFour = "SingleTimeTipTierFour"
    }

    struct MonthlyTransactions {
        static let TierOne = "TierOne"
        static let TierTwo = "TierTwo"
        static let TierThree = "TierThree"
    }

    static var getFlashCardAnswersFetchRequest: NSFetchRequest<FlashCardAnswer> {
        let request: NSFetchRequest<FlashCardAnswer> = FlashCardAnswer.fetchRequest()
        request.sortDescriptors = []
        
        return request
    }
    
    static var getFlashCardAnswersWithPredicateFetchRequest: NSFetchRequest<FlashCardAnswer> {
        let request: NSFetchRequest<FlashCardAnswer> = FlashCardAnswer.fetchRequest()
        request.sortDescriptors = []
        request.predicate = NSPredicate(format: "word == %@", "a")
        
        return request
    }
    
    static var getLessonAnswersFetchRequest: NSFetchRequest<LessonAnswer> {
        let request: NSFetchRequest<LessonAnswer> = LessonAnswer.fetchRequest()
        request.sortDescriptors = []
        
        return request
    }
}
